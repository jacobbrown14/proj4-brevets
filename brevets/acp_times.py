"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders

Edited by Jacob Brown on 11/6/2018
for Project 4, CIS 322, Fall 2018
"""
import arrow, math

# strip_iso_string() - Takes an ISO 8601 format datetime string and removes all
# the formatting nonsense. Used to create a new arrow for easier manipulation
def strip_iso_string(iso_str):

    temp1 = str(iso_str)
    temp2 = temp1.replace('<bound method Arrow.isoformat of <Arrow [', '')
    new_str = temp2.replace(']>>', '')

    return new_str

# percentage_checker() - Checks to see if the control is 20% longer than
# the brevet. If it is, it returns 1. If it's less than zero (and thus
# normal), it returns 2. If it's between 0 and 20% (and we need to round to
# a brevet), it returns 0.
def percentage_checker(control, brevet):

    # Used to prevent div-by-zero cases
    if (control > 0):
        percentage = (control - brevet) / control

        if (percentage > 0.2):
            return 1
        elif (percentage < 0):
            return 2

    return 0

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):

    # For readability's sake
    control = control_dist_km
    brevet = brevet_dist_km

    # Creates the new arrow and adjusts its timezone
    new_str = strip_iso_string(brevet_start_time)
    new_arrow = arrow.get(new_str)
    new_arrow = new_arrow.replace(tzinfo='US/Pacific')

    # We return nothing if the control is zero
    if control == 0:
        return new_arrow.isoformat()

    # Table helper ('cause if-else is poor code)
    MAX_TABLE = [ ((0, 200), 34),  ((200, 400), 32), ((400, 600), 30), ((600, 1000), 28), ((1000, 1300), 26) ]

    # Checks percentage range of control and brevet
    percentage = percentage_checker(control, brevet)

    calc = 0
    helper = 0

    # Runs through table, determines if control is within that element's range. 
    # If it isn't, it calculates using that range's speed and adds to helper.
    # If it is, it subtracts the low from control, calculates using that range's
    # speed and adds to the helper.
    for pair in MAX_TABLE:
        first, second = pair
        low, high = first
        if control > low and control <= high:
            if percentage == 1 or percentage == 2:
                calc += ((control - low) / second) + helper
            else:
                calc += helper
            break
        else:
            helper += ((high - low) / second)

    # Grabs hour and minute from calc, calculates minute portion
    # based on algorithm, and shift the time accordingly
    minute, hour = math.modf(calc)
    new_min = int(round(minute * 60))
    new_arrow = new_arrow.shift(hours = hour, minutes = new_min)

    return new_arrow.isoformat()

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):

    # For readability's sake
    control = control_dist_km
    brevet = brevet_dist_km

    # Creates the new arrow and adjusts its timezone
    new_str = strip_iso_string(brevet_start_time)
    new_arrow = arrow.get(new_str)
    new_arrow = new_arrow.replace(tzinfo='US/Pacific')

    # Sets the closing time to one hour as per the rules 
    # and returns the new arrow
    if control == 0:
        new_arrow = new_arrow.shift(hours = +1)
        return new_arrow.isoformat()

    # Table helper ('cause if-else is poor code)
    MIN_TABLE = [ ((0, 200), 15),  ((200, 400), 15), ((400, 600), 15), ((600, 1000), 11.428), ((1000, 1300), 13.333) ]
    
    # Checks percentage range of brevet and control
    percentage = percentage_checker(control, brevet)

    calc = 0
    helper = 0

    # Runs through table, determines if control is within that element's range. 
    # If it isn't, it calculates using that range's speed and adds to helper. 
    # If it is, it subtracts the low from control, calculates using that range's
    # speed and adds to the helper.
    for pair in MIN_TABLE:
        first, second = pair
        low, high = first
        if control > low and control <= high:
            if percentage == 1 or percentage == 2:
                calc += ((control - low) / second) + helper
            else:
                calc += helper
            break
        else:
            helper += ((high - low) / second)
    
    # Grabs hour and minute from calc, calculates minute portion
    # based on algorithm, and shift the time accordingly
    minute, hour = math.modf(calc)
    new_min = int(round(minute * 60))
    new_arrow = new_arrow.shift(hours = hour, minutes = new_min)

    return new_arrow.isoformat()
