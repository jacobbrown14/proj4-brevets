"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

Edited by Jacob Brown, 11/6/2018
for Project 4, CIS 322, Fall 2018
"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    # Grabs date and time strings from calc.html
    date = request.args.get('date', "", type=str)
    time = request.args.get('time', "", type=str)
    dist = request.args.get('dist', 200, type=int)

    # Formats date and time string for arrow function stuff
    date_str = date + " " + time
    start_time = arrow.get(date_str).isoformat

    open_time = acp_times.open_time(km, dist, start_time)
    close_time = acp_times.close_time(km, dist, start_time)

    # Error-checking to be displayed in "notes" column
    note = ""

    if acp_times.percentage_checker(km, dist) == 1:
        note = "Warning: Control distance is 20% or longer than brevet"

    if km == 0:
        note = "Note: Control distance is at zero"

    result = {"open": open_time, "close": close_time, "note": note}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
