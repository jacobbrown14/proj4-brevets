"""
Nose tests for acp_times.py

By Jacob Brown, 11/6/2018
for CIS 322, Project 4, Fall 2018

Test cases pulled from:
https://rusa.org/pages/acp-brevet-control-times-calculator
"""

from acp_times import open_time, close_time

import nose
import arrow
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.WARNING)
log = logging.getLogger(__name__)

# Empty date string used in each test
empty_date = "2017-01-01 00:00"

# Open times answer from Example 3
def test_open1():

    # Correct answer
    ans = "2017-01-02T05:09:00-08:00"

    # Creates an isoformat arrow
    start_time = arrow.get(empty_date).isoformat()
    new_arrow = open_time(890, 1000, start_time)

    assert str(new_arrow) == ans

# Open times answer from Example 2
def test_open2():

    # Correct answer
    ans = "2017-01-01T17:08:00-08:00"

    # Creates an isoformat arrow
    start_time = arrow.get(empty_date).isoformat()
    new_arrow = open_time(550, 600, start_time)

    assert str(new_arrow) == ans

# Close times answer from Example 3
def test_close():

    # Correct answer
    ans = "2017-01-03T17:23:00-08:00"

    # Creates an isoformat arrow
    start_time = arrow.get(empty_date).isoformat()
    new_arrow = close_time(890, 1000, start_time)

    assert str(new_arrow) == ans

# Tests 20% issue from ACP Calculator.
# Returns true if control is over 20% longer than brevet.
def test_twenty_per_cent():

    # Correct answer
    ans = "2017-01-01T05:53:00-08:00"

    # Creates an isoformat arrow
    start_time = arrow.get(empty_date).isoformat()
    new_arrow = open_time(250, 200, start_time)

    assert str(new_arrow) == ans

# Tests control at zero for close times
def test_control_zero():

    # Correct answer
    ans = "2017-01-01T01:00:00-08:00"

    # Creates an isoformat arrow
    start_time = arrow.get(empty_date).isoformat()
    new_arrow = close_time(0, 200, start_time)

    assert str(new_arrow) == ans
