# README #

Author: Jacob Brown, jbrown14@uoregon.edu
Project 4, 11/6/2018, for CIS 322, Fall 2018

## Description: 

An AJAX and Flask implementation of an ACP Brevet Control Times Calculator. Set the Brevet distance, starting time and date, and then set the control location in miles. The calculator will convert this to kilometers and display the opening times and closing times using these values. Further explanation of the algorithm can be found below (at "Algorithm")

## Instructions: 

(Docker) Make sure a credentials.ini file exists in the "brevets" directory first. Then, use start.sh to build and run this container. Go to "localhost:5000" in a browser to see the calculator.  To stop all running containers, use stop.sh.

(Python3) In the "brevets" directory, simply use "python3 flask_brevets.py" to run the program. Go to "localhost:5000" in a browser to see the calculator. Ctrl+C stops the program.

(Nosetests) In the "brevets" directory, simply run "nosetests" to check the test the validity of the functions within acp_brevets.py. If everything works, you should see five little dots.

## Intro:

(Paraphrased from Wikipedia): Randonneuring a long-distance sport in cycling. A randonneuring event is referred to as a "brevet." These brevets have checkpoints - called "controls" - throughout the length of the brevet.

## Algorithm:

Follow along using the table found here: https://rusa.org/pages/acp-brevet-control-times-calculator

- Starting from zero, for each row the control distance doesn't fall into, subtract the low range from the high range, and divide this value by its speed. Each of these values are added together.

- For the row that the control does fall into, subtract the low range from the control, then divide by its speed. Then add this value to the result from the previous step.

- The fractional portion of your result will need to be multiplied by 60, then rounded to the nearest integer. This result will represent the minutes, while the whole numbers will represent the hours. They'll follow this template: 00H00

- Add this number to your starting time to see the final open/close time.

Example (using an 890km brevet on a 1000km brevet):

For the opening times, we use values in the Maximum Speed column.

- (200 / 34) + (200 / 32) + (200 / 30) + ((890 - 600) / 28) = 29.1561624...

- 0.1561624 * 60 = 9.36972

- 29H09

For the closing times, we use the values in the Minimum Speed column.

- (200 / 15) + (200 / 15) + (200 / 15) + ((890 - 600) / 11.428) = 65.37626...

- 0.37626 * 60 = 22.576128

- 65H23

## Algorithm Misc:

- As per the rules, the closing time at the start of a brevet will always be set one hour after the starting time. A note will appear in the calculator if your control distance is set to zero.

- Control distances between 0% and 20% of the brevet distance will instead use the brevet distance in its calculation. Control distances beyond 20% of the brevet distance will calculate normally albeit incorrectly and will display an error message in the "Notes" column.
